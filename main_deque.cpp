// 2019 by Stephan Roslen

#include <deque>
#include <iostream>

int main(int argc, const char *argv[]) {
  std::deque<uint64_t> primes{};
  uint64_t cur = 2;
  for (uint64_t i{0}; i < 5000;) {
    bool isprime{true};
    for (const auto p : primes) {
      if (0 == cur % p) {
        isprime = false;
        break;
      }
      if (p * p >= cur) {
        break;
      }
    }
    if (isprime) {
      std::cout << cur << "\n";
      primes.push_back(cur);
      ++i;
    }
    ++cur;
  }
  return 0;
}