// 2019 by Stephan Roslen

#include <functional>
#include <iostream>

int main(int argc, const char *argv[]) {
  auto GetPrime = [Gen = std::function<uint64_t(void)>{
    [val = uint64_t{1}](void) mutable {
        return ++val;
      }}](void) mutable {
        uint64_t val = Gen();
        Gen = [filter = val, OldGen = Gen](void){
          uint64_t val = OldGen();
          if (val % filter == 0)
            val = OldGen();
          return val;
        };
        return val;
      };
  for (uint16_t i{0}; i < 5000 ; ++i) {
    std::cout << GetPrime() << "\n";
  }
  return 0;
}